Follow below instructions to install required software and run the application.

######Softwares Required
1. [ Download Java](https://www.java.com/en/download/)
2. [ Download Apache Flink](https://flink.apache.org/downloads.html)
4. [Download Docker](https://docs.docker.com/)
3. [ Instructions to Pull docker Image](https://hub.docker.com/_/influxdb)
4. [Maven](https://maven.apache.org/download.cgi)

#####Instructions:
As a first step, set the path for all softwares installed so that respective commands will be executed from terminal

###### Influx Database
1. Open Terminal
2. Pull the Influx docker image using command ``docker pull influxdb``
3. Run the docker image either from docker dashboard or using command ``docker run --name=influxdb -d -p 8086:8086 influxdb``
4. Run Influx DB client using command ``docker exec -it influxdb influx``
4. Create database using command ``create database anomaly_detection``

Once anomaly_detection DB is created proceed to build the jar

###### Build the Jar
1. [Clone the project](https://gitlab.com/kishorkumarkommula/flink-influx-test.git)
2. Build the Jar using command  ``mvn clean install``
3. Execute below command to run the application ``./bin/flink run <Jar location> --configFile config.local.yaml --sensorFile <sensor file physical path>``

4. After job is executed you can verify number of rows inserted into the DB using below command. ``SELECT COUNT(sensor) FROM datapoints``

