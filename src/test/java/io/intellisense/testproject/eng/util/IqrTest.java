package io.intellisense.testproject.eng.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.Test;

public class IqrTest {

	@Test
	public void testCalculateIqrWithOddNumberElements() {
		Double[] array = { 215.3042922990786, 215.62644803008027, 215.74327462666216, 215.96172626513308,
				216.41635650530833, 216.49370587701853, 216.51950156188153, 216.54402864511695, 216.56024764813185,
				216.72171640313852, 216.72071640313852 };

		ArrayList<Double> list = new ArrayList<Double>();
		Collections.addAll(list, array);
		double iqr = 0;
		iqr = Iqr.calculateIqr(list);
		assertEquals(Double.valueOf("0.8169730214696926"), iqr);

	}

	@Test
	public void testCalculateIqrWithEvenNumberElements() {
		Double[] array = { 215.3042922990786, 215.62644803008027, 215.74327462666216, 215.96172626513308, 216.41635650530833,
						216.49370587701853, 216.51950156188153, 216.54402864511695, 216.56024764813185, 216.72171640313852 };

		ArrayList<Double> list = new ArrayList<Double>();
		Collections.addAll(list, array);
		double iqr = 0;
		iqr = Iqr.calculateIqr(list);
		assertEquals(Double.valueOf("0.8007540184547963"), iqr);

	}

	@Test
	public void testCalculateIqrUnOrderedList() {
		Double[] array = { 216.56024764813185, 216.49370587701853, 216.51950156188153, 215.3042922990786,
				215.62644803008027, 215.74327462666216, 215.96172626513308, 216.41635650530833, 216.54402864511695,
				216.72171640313852, 216.72071640313852 };

		ArrayList<Double> list = new ArrayList<Double>();
		Collections.addAll(list, array);
		double iqr = 0;
		iqr = Iqr.calculateIqr(list);
		assertEquals(Double.valueOf("0.8169730214696926"), iqr);

	}
	@Test
	public void testCalculateIqrEvenNumbersForMedian() {
		Double[] array = { 215.3042922990786, 215.62644803008027, 215.74327462666216, 215.96172626513308, 216.41635650530833,
				 216.49370587701853, 216.51950156188153, 216.54402864511695, 216.56024764813185, 216.72171640313852,
				 216.72071640313852, 216.71071640313852  };

		ArrayList<Double> list = new ArrayList<Double>();
		Collections.addAll(list, array);
		double iqr = 0;
		iqr = Iqr.calculateIqr(list);
		assertEquals(Double.valueOf("0.7829815797375659"), iqr);

	}
}
