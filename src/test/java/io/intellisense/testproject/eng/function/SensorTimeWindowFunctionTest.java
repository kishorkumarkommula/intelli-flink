package io.intellisense.testproject.eng.function;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import io.intellisense.testproject.eng.model.DataPoint;

public class SensorTimeWindowFunctionTest {
	
	@Mock
	TimeWindow timeWindow;
	
	ArrayList<DataPoint> dataList= null;
	
	Collector<DataPoint> out = new Collector<DataPoint>() {
		
		@Override
		public void collect(DataPoint record) {
			dataList.add(record); 
			
		}
		
		@Override
		public void close() {
			dataList= null;
		}
	};	

	SensorTimeWindowFunction sensorTimeWindowFunction= null;
	@Test
	public void testApply_CollecteDataSize() {
		dataList=new ArrayList<DataPoint>();
		sensorTimeWindowFunction = new SensorTimeWindowFunction();
		Iterable<Row> values = buildInitialList();
		try {
			sensorTimeWindowFunction.apply(timeWindow, values, out);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(20, dataList.size());
		long time = Long.valueOf("1483228800000");
		assertEquals(time, dataList.get(0).getTime());
		dataList.forEach(datapoint -> {
			assertEquals(1, datapoint.getScore());
		});
	}
	@Test
	public void testApply_Collectedata() {
		dataList=new ArrayList<DataPoint>();
		sensorTimeWindowFunction = new SensorTimeWindowFunction();
		ArrayList<Row> list = buildInitialList();
		Row row = new Row(3);
		row.setField(0, "2017-01-01T00:10:00Z");
		row.setField(1, "");
		row.setField(2, 82.08445695825746);
		list.add(row);
		Iterable<Row> values = list;
		try {
			sensorTimeWindowFunction.apply(timeWindow, values, out);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(21, dataList.size());
		
	}
	private ArrayList<Row> buildInitialList(){
		String[] timeStamp = {"2017-01-01T00:00:00Z", "2017-01-01T00:01:00Z", "2017-01-01T00:02:00Z", "2017-01-01T00:03:00Z",
				"2017-01-01T00:04:00Z", "2017-01-01T00:05:00Z", "2017-01-01T00:06:00Z", "2017-01-01T00:07:00Z",
				"2017-01-01T00:08:00Z", "2017-01-01T00:09:00Z"};
		Double[] sensorOne = { 215.3042922990786, 215.62644803008027, 215.74327462666216, 215.96172626513308, 216.41635650530833,
				 216.49370587701853, 216.51950156188153, 216.54402864511695, 216.56024764813185, 216.72171640313852 };
		Double[] sensortwo ={82.01029574196714, 82.02806759560941, 82.03962790902443, 82.04641559698796, 82.05714609367288,
				82.0607614424642, 82.068350376747, 82.070664371301, 82.07762616728168, 82.08445695825746};
		ArrayList<Row> list = new ArrayList<Row>();
		
		for(int i=0;i<10;i++) {
			Row row = new Row(3);
			row.setField(0, timeStamp[i]); 
			row.setField(1, sensorOne[i]); 
			row.setField(2, sensortwo[i]); 
			list.add(row);
		}
		return list;
	}

}
