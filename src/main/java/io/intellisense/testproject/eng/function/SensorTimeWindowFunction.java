package io.intellisense.testproject.eng.function;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;

import io.intellisense.testproject.eng.model.DataPoint;
import io.intellisense.testproject.eng.util.Iqr;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SensorTimeWindowFunction implements AllWindowFunction<Row, DataPoint, TimeWindow> {

	private static final long serialVersionUID = 1L;

	/**
	 * This method will invoke the calculateIqr to get the iqr for each sensor and
	 * hold iqr value in map
	 * 
	 * @param sensorMap
	 * @return iqr for each sensor
	 */
	private static Map<Integer, Double> getIqr(Map<Integer, List<Double>> sensorMap) {

		Set<Integer> sensorNumbers = sensorMap.keySet();

		Map<Integer, Double> mapIQr = new HashMap<Integer, Double>();

		for (Integer sensorNumber : sensorNumbers) {

			ArrayList<Double> sensorValues = (ArrayList<Double>) sensorMap.get(sensorNumber);

			mapIQr.put(sensorNumber, Iqr.calculateIqr(sensorValues));

		}

		return mapIQr;

	}

	/**
	 * This method will derive the score based on the sensor value an iqr of window
	 * elements
	 * 
	 * @param sensorValue
	 * @param iqr
	 * @return score
	 */
	private static double applyScore(Double sensorValue, Double iqr) {

		Double tempScore = 1.5 * iqr;

		if (sensorValue < tempScore)
			return 0;

		if (sensorValue >= tempScore && sensorValue < (3 * iqr))
			return 0.5;

		if (sensorValue >= (3 * iqr))
			return 1;

		return 0;

	}

	/**
	 * The apply method is called for each windowed elements. All elements within
	 * the window will be available as iterable elements. The method will have the
	 * resulted elements in a stream
	 * 
	 * @param timeWindow which will give you access to functionalities such as start
	 *                   offset of the window, merge windows etc..
	 * @param rows       row of iterable elements which will be used for actual
	 *                   processing
	 * @param collector  output will be store in collector
	 */
	@Override
	public void apply(TimeWindow timeWindow, Iterable<Row> rows, Collector<DataPoint> collector) {

		Map<Integer, List<Double>> sensorListMap = new HashMap<>();

		List<String> timeStampList = new ArrayList<>();

		for (Row row : rows) {

			int rowCount = row.getArity();

			timeStampList.add(row.getField(0).toString());

			for (int i = 1; i < rowCount; i++) {
				Object coulmnDataObject = row.getField(i);
				String coulmnData = coulmnDataObject != null ? coulmnDataObject.toString().trim() : null;
				if (coulmnData != null && !"".equals(coulmnData)) {
					if (sensorListMap.containsKey(i)) {

						List<Double> data = sensorListMap.get(i);

						data.add(Double.valueOf(coulmnData));

						sensorListMap.put(i, data);

					} else {

						List<Double> data = new ArrayList<>();

						data.add(Double.valueOf(coulmnData));

						sensorListMap.put(i, data);
					}
				}
			}
		}

		Map<Integer, Double> iqrMap = getIqr(sensorListMap);

		for (Integer sensorNumber : iqrMap.keySet()) {

			Double iqr = iqrMap.get(sensorNumber);
			log.info("IQR Value for Sensor:" + sensorNumber + " is:" + iqr);
			List<Double> individualSensorList = sensorListMap.get(sensorNumber);

			int timestampRow = 0;

			for (Double scensorValue : individualSensorList) {
				long time = Instant.parse(timeStampList.get(timestampRow)).toEpochMilli();
				DataPoint dataPoint = new DataPoint(time, "sensor-" + sensorNumber, scensorValue,
						applyScore(scensorValue, iqr));
				timestampRow += 1;
				collector.collect(dataPoint);

			}

		}
	}

}
