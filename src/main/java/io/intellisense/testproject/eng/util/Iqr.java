package io.intellisense.testproject.eng.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Iqr {

	/**
	 * calculate Iqr
	 * @param dataList
	 * @return iqr value
	 */
	public static Double calculateIqr(ArrayList<Double> dataList){
		@SuppressWarnings("unchecked")
		ArrayList<Double> data = (ArrayList<Double>) dataList.clone();
		Collections.sort(data);
		double q1 = findMedian(data, 0, data.size() / 2 - 1);

		double q3 = findMedian(data, (data.size() + 1) / 2, data.size() - 1);

		return (q3 - q1);

	}

	/**
	 * this method will find the median value for array of values
	 * @param sensorValues
	 * @param startInex
	 * @param endInex
	 * @return median
	 */
	private static double findMedian(List<Double> sensorValues, int startInex, int endInex) {

		if ((endInex - startInex) % 2 == 0) {

			return (sensorValues.get((endInex + startInex) / 2));

		} else {

			double value1 = sensorValues.get((endInex + startInex) / 2);

			double value2 = sensorValues.get((endInex + startInex) / 2 + 1);

			return (value1 + value2) / 2.0;

		}

	}
}
