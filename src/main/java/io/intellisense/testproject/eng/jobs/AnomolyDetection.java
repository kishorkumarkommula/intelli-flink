package io.intellisense.testproject.eng.jobs;

import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.table.sources.CsvTableSource;
import org.apache.flink.types.Row;

import io.intellisense.testproject.eng.datasource.CsvDatasource;
import io.intellisense.testproject.eng.function.SensorTimeWindowFunction;
import io.intellisense.testproject.eng.model.DataPoint;
import io.intellisense.testproject.eng.sink.InfluxDBSink;

public class AnomolyDetection {

	/**
	 * This method extract event time
	 * @param event
	 * @return event time
	 */
	private static long timestampExtract(Row event) {

		final String timestampField = (String) event.getField(0);

		return Instant.parse(timestampField).toEpochMilli();

	}

	/**
	 * main method will be invoked when the job is triggered, 
	 * this method will assign the anomaly score for each sensor value and store in influx db
	 * @param args
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		final ParameterTool params = ParameterTool.fromArgs(args);

		final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

		final String configFile = params.get("configFile", "config.local.yaml");
		String dataset =params.getRequired("sensorFile");
        final InputStream resourceStream = AnomolyDetection.class.getClassLoader().getResourceAsStream(configFile);
        final ParameterTool configProperties = ParameterTool.fromPropertiesFile(resourceStream);
		final CsvTableSource csvDataSource = CsvDatasource.of(dataset).getCsvSource();

		final WatermarkStrategy<Row> watermarkStrategy = WatermarkStrategy.<Row>forMonotonousTimestamps()

				.withTimestampAssigner((event, timestamp) -> timestampExtract(event));
	

		final DataStream<Row> sourceStream = csvDataSource.getDataStream(env)

				.assignTimestampsAndWatermarks(watermarkStrategy).name("datasource-operator");


		DataStream<DataPoint> dataStream = sourceStream.timeWindowAll(Time.minutes(120), Time.minutes(100)).apply(new SensorTimeWindowFunction());
		
		//DataStream<DataPoint> dataStream = sourceStream.countWindowAll(100).apply(new SensorCountWindowFunction());

		//rowstrm.print();
        final SinkFunction<?> influxDbSink = new InfluxDBSink<>(configProperties);
        dataStream.addSink((SinkFunction<DataPoint>) influxDbSink).name("influx-sink");
        
		env.execute();

	}

}
