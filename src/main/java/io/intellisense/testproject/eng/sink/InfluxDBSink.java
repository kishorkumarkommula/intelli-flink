package io.intellisense.testproject.eng.sink;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import io.intellisense.testproject.eng.model.DataPoint;

import java.util.concurrent.TimeUnit;

import org.apache.flink.api.common.accumulators.Accumulator;
import org.apache.flink.api.common.accumulators.IntCounter;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.influxdb.BatchOptions;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

@Slf4j
@RequiredArgsConstructor
public class InfluxDBSink<T extends DataPoint> extends RichSinkFunction<T> {

	private static final long serialVersionUID = 1L;

	transient InfluxDB influxDB;

    final ParameterTool configProperties;

    final Accumulator<Integer, Integer> recordsIn = new IntCounter(0);
    final Accumulator<Integer, Integer> recordsOut = new IntCounter(0);

	@Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        getRuntimeContext().addAccumulator(getClass().getSimpleName() + "-recordsIn", recordsIn);
        getRuntimeContext().addAccumulator(getClass().getSimpleName() + "-recordsOut", recordsOut);
        influxDB = InfluxDBFactory.connect(
        		configProperties.get("influxdb.url"),
        		configProperties.get("influxdb.username"), 
        		configProperties.get("influxdb.password"));
        influxDB.setDatabase(configProperties.get("influxdb.dbName"));
        BatchOptions options = BatchOptions.DEFAULTS.actions(1000).flushDuration(1000);
        influxDB.enableBatch(options);
    }


    @Override
    public void invoke(T dataPoint, Context context) {
        recordsIn.add(1);
        try {
            final Point point = Point.measurementByPOJO(DataPoint.class)
            		.tag("sensorName", dataPoint.getSensor())
            		.time(dataPoint.getTime(), TimeUnit.MILLISECONDS)
            		  .addField("sensor", dataPoint.getSensor())
            		  .addField("value", dataPoint.getValue())
            		  .addField("score", dataPoint.getScore())
                    .build();
            influxDB.write(point);
            recordsOut.add(1);
        } catch(Exception e) {
            log.error(e.getMessage());
        }
    }
}